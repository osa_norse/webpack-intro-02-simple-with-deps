import $ from 'jquery';

import { cube } from './libs/math.js';
import { capitalize } from './libs/strings.js';

$('body').append(`<div>Cube of 3 is: ${cube(3)}</div>`);

$('body').append(`<div>Capitalize of <code>john</code> is: <code>${capitalize('john')}</code></div>`);
